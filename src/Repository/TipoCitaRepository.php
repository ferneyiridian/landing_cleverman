<?php

namespace App\Repository;

use App\Entity\TipoCita;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TipoCita|null find($id, $lockMode = null, $lockVersion = null)
 * @method TipoCita|null findOneBy(array $criteria, array $orderBy = null)
 * @method TipoCita[]    findAll()
 * @method TipoCita[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TipoCitaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TipoCita::class);
    }

    // /**
    //  * @return TipoCita[] Returns an array of TipoCita objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TipoCita
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
