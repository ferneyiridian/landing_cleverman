<?php

namespace App\Repository;

use App\Entity\ServiciosAfiliados;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ServiciosAfiliados|null find($id, $lockMode = null, $lockVersion = null)
 * @method ServiciosAfiliados|null findOneBy(array $criteria, array $orderBy = null)
 * @method ServiciosAfiliados[]    findAll()
 * @method ServiciosAfiliados[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ServiciosAfiliadosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ServiciosAfiliados::class);
    }

    // /**
    //  * @return ServiciosAfiliados[] Returns an array of ServiciosAfiliados objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ServiciosAfiliados
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
