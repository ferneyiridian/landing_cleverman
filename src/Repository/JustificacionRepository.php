<?php

namespace App\Repository;

use App\Entity\Justificacion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Justificacion|null find($id, $lockMode = null, $lockVersion = null)
 * @method Justificacion|null findOneBy(array $criteria, array $orderBy = null)
 * @method Justificacion[]    findAll()
 * @method Justificacion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JustificacionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Justificacion::class);
    }

    // /**
    //  * @return Justificacion[] Returns an array of Justificacion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Justificacion
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
