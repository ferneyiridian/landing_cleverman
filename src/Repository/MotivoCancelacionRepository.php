<?php

namespace App\Repository;

use App\Entity\MotivoCancelacion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method MotivoCancelacion|null find($id, $lockMode = null, $lockVersion = null)
 * @method MotivoCancelacion|null findOneBy(array $criteria, array $orderBy = null)
 * @method MotivoCancelacion[]    findAll()
 * @method MotivoCancelacion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MotivoCancelacionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MotivoCancelacion::class);
    }

    // /**
    //  * @return MotivoCancelacion[] Returns an array of MotivoCancelacion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MotivoCancelacion
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
