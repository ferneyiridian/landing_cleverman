<?php

namespace App\Repository;

use App\Entity\Tagfaq;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Tagfaq|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tagfaq|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tagfaq[]    findAll()
 * @method Tagfaq[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TagfaqRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Tagfaq::class);
    }

    // /**
    //  * @return Tagfaq[] Returns an array of Tagfaq objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Tagfaq
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
