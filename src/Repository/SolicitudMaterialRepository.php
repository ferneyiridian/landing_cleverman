<?php

namespace App\Repository;

use App\Entity\SolicitudMaterial;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method SolicitudMaterial|null find($id, $lockMode = null, $lockVersion = null)
 * @method SolicitudMaterial|null findOneBy(array $criteria, array $orderBy = null)
 * @method SolicitudMaterial[]    findAll()
 * @method SolicitudMaterial[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SolicitudMaterialRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SolicitudMaterial::class);
    }

    // /**
    //  * @return SolicitudMaterial[] Returns an array of SolicitudMaterial objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SolicitudMaterial
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
