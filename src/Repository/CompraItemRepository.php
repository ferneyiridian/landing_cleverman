<?php

namespace App\Repository;

use App\Entity\CompraItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CompraItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompraItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompraItem[]    findAll()
 * @method CompraItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompraItemRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CompraItem::class);
    }

    // /**
    //  * @return CompraItem[] Returns an array of CompraItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CompraItem
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
