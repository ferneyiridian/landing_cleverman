<?php

namespace App\Repository;

use App\Entity\Distribuidor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Distribuidor|null find($id, $lockMode = null, $lockVersion = null)
 * @method Distribuidor|null findOneBy(array $criteria, array $orderBy = null)
 * @method Distribuidor[]    findAll()
 * @method Distribuidor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DistribuidorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Distribuidor::class);
    }

    // /**
    //  * @return Distribuidor[] Returns an array of Distribuidor objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Distribuidor
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
