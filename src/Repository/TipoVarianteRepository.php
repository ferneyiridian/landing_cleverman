<?php

namespace App\Repository;

use App\Entity\TipoVariante;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TipoVariante|null find($id, $lockMode = null, $lockVersion = null)
 * @method TipoVariante|null findOneBy(array $criteria, array $orderBy = null)
 * @method TipoVariante[]    findAll()
 * @method TipoVariante[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TipoVarianteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TipoVariante::class);
    }

    // /**
    //  * @return TipoVariante[] Returns an array of TipoVariante objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TipoVariante
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
