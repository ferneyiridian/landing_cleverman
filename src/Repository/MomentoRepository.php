<?php

namespace App\Repository;

use App\Entity\Momento;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Momento|null find($id, $lockMode = null, $lockVersion = null)
 * @method Momento|null findOneBy(array $criteria, array $orderBy = null)
 * @method Momento[]    findAll()
 * @method Momento[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MomentoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Momento::class);
    }

    // /**
    //  * @return Momento[] Returns an array of Momento objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Momento
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
