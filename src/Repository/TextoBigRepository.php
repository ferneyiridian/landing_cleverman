<?php

namespace App\Repository;

use App\Entity\TextoBig;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TextoBig|null find($id, $lockMode = null, $lockVersion = null)
 * @method TextoBig|null findOneBy(array $criteria, array $orderBy = null)
 * @method TextoBig[]    findAll()
 * @method TextoBig[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TextoBigRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TextoBig::class);
    }

    // /**
    //  * @return TextoBig[] Returns an array of TextoBig objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TextoBig
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
