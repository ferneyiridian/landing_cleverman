<?php

namespace App\Repository;

use App\Entity\LaunchRegistration;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LaunchRegistration|null find($id, $lockMode = null, $lockVersion = null)
 * @method LaunchRegistration|null findOneBy(array $criteria, array $orderBy = null)
 * @method LaunchRegistration[]    findAll()
 * @method LaunchRegistration[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LaunchRegistrationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LaunchRegistration::class);
    }

    // /**
    //  * @return LaunchRegistration[] Returns an array of LaunchRegistration objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LaunchRegistration
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
