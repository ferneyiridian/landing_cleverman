<?php

namespace App\Repository;

use App\Entity\SuperGrupo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SuperGrupo|null find($id, $lockMode = null, $lockVersion = null)
 * @method SuperGrupo|null findOneBy(array $criteria, array $orderBy = null)
 * @method SuperGrupo[]    findAll()
 * @method SuperGrupo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SuperGrupoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SuperGrupo::class);
    }

    // /**
    //  * @return SuperGrupo[] Returns an array of SuperGrupo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SuperGrupo
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
