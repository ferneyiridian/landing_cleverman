<?php

namespace App\Repository;

use App\Entity\TipoGasto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TipoGasto|null find($id, $lockMode = null, $lockVersion = null)
 * @method TipoGasto|null findOneBy(array $criteria, array $orderBy = null)
 * @method TipoGasto[]    findAll()
 * @method TipoGasto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TipoGastoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TipoGasto::class);
    }

    // /**
    //  * @return TipoGasto[] Returns an array of TipoGasto objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TipoGasto
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
