<?php

namespace App\Repository;

use App\Entity\CancelacionCita;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CancelacionCita|null find($id, $lockMode = null, $lockVersion = null)
 * @method CancelacionCita|null findOneBy(array $criteria, array $orderBy = null)
 * @method CancelacionCita[]    findAll()
 * @method CancelacionCita[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CancelacionCitaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CancelacionCita::class);
    }

    // /**
    //  * @return CancelacionCita[] Returns an array of CancelacionCita objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CancelacionCita
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
