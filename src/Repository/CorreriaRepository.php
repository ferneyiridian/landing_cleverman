<?php

namespace App\Repository;

use App\Entity\Correria;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Correria|null find($id, $lockMode = null, $lockVersion = null)
 * @method Correria|null findOneBy(array $criteria, array $orderBy = null)
 * @method Correria[]    findAll()
 * @method Correria[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CorreriaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Correria::class);
    }

    // /**
    //  * @return Correria[] Returns an array of Correria objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Correria
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
