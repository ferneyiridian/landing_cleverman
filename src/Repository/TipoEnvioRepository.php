<?php

namespace App\Repository;

use App\Entity\TipoEnvio;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TipoEnvio|null find($id, $lockMode = null, $lockVersion = null)
 * @method TipoEnvio|null findOneBy(array $criteria, array $orderBy = null)
 * @method TipoEnvio[]    findAll()
 * @method TipoEnvio[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TipoEnvioRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TipoEnvio::class);
    }

    // /**
    //  * @return TipoEnvio[] Returns an array of TipoEnvio objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TipoEnvio
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
