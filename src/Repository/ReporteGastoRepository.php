<?php

namespace App\Repository;

use App\Entity\ReporteGasto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ReporteGasto|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReporteGasto|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReporteGasto[]    findAll()
 * @method ReporteGasto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReporteGastoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReporteGasto::class);
    }

    // /**
    //  * @return ReporteGasto[] Returns an array of ReporteGasto objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReporteGasto
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
