<?php
/**
 * Created by PhpStorm.
 * User: Mauricio
 * Date: 20/05/2019
 * Time: 4:33 PM
 */

namespace App\Form;


use App\Entity\ItemPromocion;
use App\Entity\Producto;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ItemPromocionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('producto',EntityType::class,array(
                'class' => Producto::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('d')
                        ->orderBy('d.nombre', 'ASC');
                }
            ))
            ->add('cantidad', TextType::class, ['attr' => ['placeholder' => 1], 'empty_data' => 1, 'required' => false]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ItemPromocion::class
        ]);
    }
}
