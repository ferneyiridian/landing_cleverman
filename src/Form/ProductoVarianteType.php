<?php
/**
 * Created by PhpStorm.
 * User: Mauricio
 * Date: 20/05/2019
 * Time: 4:33 PM
 */

namespace App\Form;


use App\Entity\Producto;
use App\Entity\Variante;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductoVarianteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', TextType::class, array( ))
            ->add('nombre', null, array('help'=>'Solo si cambia'))
            ->add('variantes', EntityType::class, [
                'class' => Variante::class,
                'label'     => 'Variación',
                'expanded'  => true,
                'multiple'  => true,
            ], array('label'=>'Variación'))
            ->add('orden', null, ['data' => 1])
            ->add('visible', null, ['data' => true])
            //->add('titulo')
            //->add('descripcion')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Producto::class,
            'constraints' => new UniqueEntity(array('fields'=>'id','message'=>'El id desbe ser único', "errorPath"=>"id"))
        ]);
    }
}
