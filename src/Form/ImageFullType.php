<?php
/**
 * Created by PhpStorm.
 * User: Mauricio
 * Date: 20/05/2019
 * Time: 4:33 PM
 */

namespace App\Form;


use App\Entity\Image;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ImageFullType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('imageFile', VichImageType::class, array('required'=>false))
            ->add('orden', null, ['data' => 1])
            ->add('visible', null, ['data' => true])
            ->add('titulo_es')
            ->add('titulo_en')
            ->add('titulo_fr')
            ->add('descripcion_es')
            ->add('descripcion_en')
            ->add('descripcion_fr')
            ->add('link', null )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Image::class,
        ]);
    }
}