<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ItemPromocionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ItemPromocion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto")
     * @ORM\JoinColumn(nullable=false)
     */
    private $producto;

    /**
     * @ORM\Column(type="integer")
     */
    private $cantidad = 1;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Promocion", inversedBy="items")
     * @ORM\JoinColumn(nullable=false)
     */
    private $promocion;

    /**
     * ItemPromocion constructor.
     */
    public function __construct()
    {
        $this->cantidad = 1;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProducto(): ?Producto
    {
        return $this->producto;
    }

    public function setProducto(?Producto $producto): self
    {
        $this->producto = $producto;

        return $this;
    }

    public function getCantidad(): ?int
    {
        return $this->cantidad;
    }

    public function setCantidad(int $cantidad): self
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    public function getPromocion(): ?Promocion
    {
        return $this->promocion;
    }

    public function setPromocion(?Promocion $promocion): self
    {
        $this->promocion = $promocion;

        return $this;
    }


    /**
     * @ORM\PostUpdate
     */
    public function onPostChanged(\Doctrine\ORM\Event\LifecycleEventArgs $args)
    {
        $em = $args->getEntityManager();
        $this->promocion->setUpdatedAt(new \DateTime("now"));
        $em->merge($this->promocion);
        $em->flush();
    }

}
