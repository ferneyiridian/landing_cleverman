<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductoRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields={"id"},message="El id debe ser único", errorPath="id")
 */

class Producto
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=100, name="id", unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $thumbnail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imagen;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $precio;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $destacado;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $orden = 1;

    /**
     * @ORM\Column(type="boolean")
     */
    private $visible = true;


    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $updatedAt;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Image", mappedBy="producto", cascade={"persist"})
     */
    private $imagenes;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Archivo", mappedBy="producto", cascade={"persist"})
     */
    private $manuales;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Video", mappedBy="producto", cascade={"persist"})
     */
    private $videos;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $ficha_tecnica;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Archivo", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $ficha_pdf;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Image", inversedBy="productos", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $descuento;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto")
     * @ORM\JoinColumn(nullable=true)
     */
    private $padre;



    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Producto", mappedBy="padre", cascade={"persist"})
     */
    private $productos;

    /**
     * @ORM\Column(type="integer")
     */
    private $iva = 19;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Subcategoria", inversedBy="productos")
     */
    private $subcategoria;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Journey", inversedBy="productos")
     */
    private $journeys;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Journey", mappedBy="libro", orphanRemoval=true)
     */
    private $journeyslibro;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Journey", mappedBy="ropa", orphanRemoval=true)
     */
    private $journeysropa;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Journey", mappedBy="gafas", orphanRemoval=true)
     */
    private $journeysgafa;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Favorito", mappedBy="producto")
     */
    private $favoritos;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Variante", inversedBy="productos")
     */
    private $variantes;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nombre_es;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nombre_en;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nombre_fr;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $resumen_es;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $resumen_en;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $resumen_fr;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descripcion_es;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descripcion_en;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descripcion_fr;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $detalle_es;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $detalle_en;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $detalle_fr;

      /**
     * @return Collection|Producto[]
     */
    public function getProductos(): Collection
    {
        return $this->productos;
    }

    public function addProducto(Producto $producto): self
    {
        if (!$this->productos->contains($producto)) {
            $this->productos[] = $producto;
            $producto->setPadre($this);
        }

        return $this;
    }

    public function removeProducto(Producto $producto): self
    {
        if ($this->productos->contains($producto)) {
            $this->productos->removeElement($producto);
            // set the owning side to null (unless already changed)
            if ($producto->getPadre() === $this) {
                $producto->setPadre(null);
            }
        }

        return $this;
    }

    public function __construct()
    {
        $this->updatedAt = new \DateTime("now");
        $this->imagenes = new ArrayCollection();
        $this->manuales = new ArrayCollection();
        $this->videos = new ArrayCollection();
        $this->productos = new ArrayCollection();
        $this->ropa = new ArrayCollection();
        $this->journeysropa = new ArrayCollection();
        $this->journeysgafa = new ArrayCollection();
        $this->favoritos = new ArrayCollection();
        $this->variantes = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getNombre(): ?string
    {
        $locale = $GLOBALS['request']->getLocale();
        $field = 'nombre_'.$locale;
        return $this->{$field};
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getResumen(): ?string
    {
        $locale = $GLOBALS['request']->getLocale();
        $field = 'resumen_'.$locale;
        return $this->{$field};
    }

    public function setResumen(?string $resumen): self
    {
        $this->resumen = $resumen;

        return $this;
    }

    public function getThumbnail(): ?string
    {
        return $this->thumbnail;
    }

    public function setThumbnail(?string $thumbnail): self
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    public function getImagen(): ?string
    {
        return $this->imagen;
    }

    public function setImagen(?string $imagen): self
    {
        $this->imagen = $imagen;

        return $this;
    }

    public function getCosto(): ?float
    {
        return $this->costo;
    }

    public function setCosto(?float $costo): self
    {
        $this->costo = $costo;

        return $this;
    }

    public function getPrecio(): ?float
    {
        return $this->precio;
    }

    public function setPrecio(float $precio): self
    {
        $this->precio = $precio;

        return $this;
    }

    public function getDestacado(): ?bool
    {
        return $this->destacado;
    }

    public function setDestacado(?bool $destacado): self
    {
        $this->destacado = $destacado;

        return $this;
    }

    public function getOrden(): ?int
    {
        return $this->orden;
    }

    public function setOrden(int $orden): self
    {
        $this->orden = $orden;

        return $this;
    }

    public function getVisible(): ?bool
    {
        return $this->visible;
    }

    public function setVisible(bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }



    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt($updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Gets triggered only on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->updatedAt = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime("now");
    }

    public function __toString()
    {
        return $this->getNombre().' ';
    }

    public function getVideo(): ?string
    {
        return $this->video;
    }

    public function setVideo(?string $video): self
    {
        $this->video = $video;

        return $this;
    }


    /**
     * @return Collection|Image[]
     */
    public function getImagenes(): Collection
    {
        return $this->imagenes;
    }

    public function addImagene(Image $imagene): self
    {
        if (!$this->imagenes->contains($imagene)) {
            $this->imagenes[] = $imagene;
            $imagene->setProducto($this);
        }

        return $this;
    }

    public function removeImagene(Image $imagene): self
    {
        if ($this->imagenes->contains($imagene)) {
            $this->imagenes->removeElement($imagene);
            // set the owning side to null (unless already changed)
            if ($imagene->getProducto() === $this) {
                $imagene->setProducto(null);
            }
        }

        return $this;
    }

    public function getDescripcion(): ?string
    {
        $locale = $GLOBALS['request']->getLocale();
        $field = 'descripcion_'.$locale;
        return $this->{$field};
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getDetalles(): ?string
    {
        $locale = $GLOBALS['request']->getLocale();
        $field = 'detalle_'.$locale;
        return $this->{$field};
    }

    public function setDetalles(?string $detalles): self
    {
        $this->detalles = $detalles;

        return $this;
    }

    /**
     * @return Collection|Archivo[]
     */
    public function getManuales(): Collection
    {
        return $this->manuales;
    }

    public function addManuale(Archivo $manuale): self
    {
        if (!$this->manuales->contains($manuale)) {
            $this->manuales[] = $manuale;
            $manuale->setProducto($this);
        }

        return $this;
    }

    public function removeManuale(Archivo $manuale): self
    {
        if ($this->manuales->contains($manuale)) {
            $this->manuales->removeElement($manuale);
            // set the owning side to null (unless already changed)
            if ($manuale->getProducto() === $this) {
                $manuale->setProducto(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Video[]
     */
    public function getVideos(): Collection
    {
        return $this->videos;
    }

    public function addVideo(Video $video): self
    {
        if (!$this->videos->contains($video)) {
            $this->videos[] = $video;
            $video->setProducto($this);
        }

        return $this;
    }

    public function removeVideo(Video $video): self
    {
        if ($this->videos->contains($video)) {
            $this->videos->removeElement($video);
            // set the owning side to null (unless already changed)
            if ($video->getProducto() === $this) {
                $video->setProducto(null);
            }
        }

        return $this;
    }

    public function getFichaTecnica(): ?string
    {
        return $this->ficha_tecnica;
    }

    public function setFichaTecnica(?string $ficha_tecnica): self
    {
        $this->ficha_tecnica = $ficha_tecnica;

        return $this;
    }

    public function getFichaPdf(): ?Archivo
    {
        return $this->ficha_pdf;
    }

    public function setFichaPdf(?Archivo $ficha_pdf): self
    {
        $this->ficha_pdf = $ficha_pdf;

        return $this;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    public function setImage(?Image $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getDescuento(): ?int
    {
        return $this->descuento;
    }

    public function setDescuento(?int $descuento): self
    {
        $this->descuento = $descuento;

        return $this;
    }

    public function getPadre(): ?self
    {
        return $this->padre;
    }

    public function setPadre(?self $padre): self
    {
        $this->padre = $padre;

        return $this;
    }





    public function getVariaciones(): ?string
    {
        $cad = '<ul>';
        foreach ($this->productos as $producto){
            $cad .= "<li>".$producto->getId()." - ".$producto->getVariantesText()."</li>";
        }
        $cad .= '</ul>';
        return $cad;
    }

    public function getIva(): ?int
    {
        return $this->iva;
    }

    public function setIva(int $iva): self
    {
        $this->iva = $iva;

        return $this;
    }

    public function getPrecioBase(){
        return $this->precio/(1+($this->iva/100));
    }

    public function getImpuesto(){
        return $this->getPrecioBase()*($this->iva/100);
    }

    public function getSubcategoria(): ?Subcategoria
    {
        return $this->subcategoria;
    }

    public function setSubcategoria(?Subcategoria $subcategoria): self
    {
        $this->subcategoria = $subcategoria;

        return $this;
    }

    /**
     * @return Collection|Journey[]
     */
    public function getJourneys(): Collection
    {
        return $this->journeys;
    }

    public function addJourney(Journey $journey): self
    {
        if (!$this->journeys->contains($journey)) {
            $this->journeys[] = $journey;
        }

        return $this;
    }

    public function removeJourney(Journey $journey): self
    {
        if ($this->journeys->contains($journey)) {
            $this->journeys->removeElement($journey);
        }

        return $this;
    }

    /**
     * @return Collection|Journey[]
     */
    public function getRopa(): Collection
    {
        return $this->ropa;
    }

    public function addRopa(Journey $ropa): self
    {
        if (!$this->ropa->contains($ropa)) {
            $this->ropa[] = $ropa;
            $ropa->setLibro($this);
        }

        return $this;
    }

    public function removeRopa(Journey $ropa): self
    {
        if ($this->ropa->contains($ropa)) {
            $this->ropa->removeElement($ropa);
            // set the owning side to null (unless already changed)
            if ($ropa->getLibro() === $this) {
                $ropa->setLibro(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Journey[]
     */
    public function getJourneysropa(): Collection
    {
        return $this->journeysropa;
    }

    public function addJourneysropa(Journey $journeysropa): self
    {
        if (!$this->journeysropa->contains($journeysropa)) {
            $this->journeysropa[] = $journeysropa;
            $journeysropa->setRopa($this);
        }

        return $this;
    }

    public function removeJourneysropa(Journey $journeysropa): self
    {
        if ($this->journeysropa->contains($journeysropa)) {
            $this->journeysropa->removeElement($journeysropa);
            // set the owning side to null (unless already changed)
            if ($journeysropa->getRopa() === $this) {
                $journeysropa->setRopa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Journey[]
     */
    public function getJourneysgafa(): Collection
    {
        return $this->journeysgafa;
    }

    public function addJourneysgafa(Journey $journeysgafa): self
    {
        if (!$this->journeysgafa->contains($journeysgafa)) {
            $this->journeysgafa[] = $journeysgafa;
            $journeysgafa->setGafas($this);
        }

        return $this;
    }

    public function removeJourneysgafa(Journey $journeysgafa): self
    {
        if ($this->journeysgafa->contains($journeysgafa)) {
            $this->journeysgafa->removeElement($journeysgafa);
            // set the owning side to null (unless already changed)
            if ($journeysgafa->getGafas() === $this) {
                $journeysgafa->setGafas(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Favorito[]
     */
    public function getFavoritos(): Collection
    {
        return $this->favoritos;
    }

    public function addFavorito(Favorito $favorito): self
    {
        if (!$this->favoritos->contains($favorito)) {
            $this->favoritos[] = $favorito;
            $favorito->setProducto($this);
        }

        return $this;
    }

    public function removeFavorito(Favorito $favorito): self
    {
        if ($this->favoritos->contains($favorito)) {
            $this->favoritos->removeElement($favorito);
            // set the owning side to null (unless already changed)
            if ($favorito->getProducto() === $this) {
                $favorito->setProducto(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Variante[]
     */
    public function getVariantes(): Collection
    {
        return $this->variantes;
    }

    public function setVariantes($variantes)
    {
        $this->variantes=$variantes;
    }

    public function addVariante(Variante $variante): self
    {
        if (!$this->variantes->contains($variante)) {
            $this->variantes[] = $variante;
        }

        return $this;
    }

    public function removeVariante(Variante $variante): self
    {
        if ($this->variantes->contains($variante)) {
            $this->variantes->removeElement($variante);
        }

        return $this;
    }

    public function getVariantesText(){
        $ret= "";

        foreach ($this->variantes as $v){
            $ret.= " ".$v->getValor()." |";
        }
        return rtrim($ret,'|');
    }

    public function getNombreEs(): ?string
    {
        return $this->nombre_es;
    }

    public function setNombreEs(?string $nombre_es): self
    {
        $this->nombre_es = $nombre_es;

        return $this;
    }

    public function getNombreEn(): ?string
    {
        return $this->nombre_en;
    }

    public function setNombreEn(?string $nombre_en): self
    {
        $this->nombre_en = $nombre_en;

        return $this;
    }

    public function getNombreFr(): ?string
    {
        return $this->nombre_fr;
    }

    public function setNombreFr(?string $nombre_fr): self
    {
        $this->nombre_fr = $nombre_fr;

        return $this;
    }

    public function getResumenEs(): ?string
    {
        return $this->resumen_es;
    }

    public function setResumenEs(?string $resumen_es): self
    {
        $this->resumen_es = $resumen_es;

        return $this;
    }

    public function getResumenEn(): ?string
    {
        return $this->resumen_en;
    }

    public function setResumenEn(?string $resumen_en): self
    {
        $this->resumen_en = $resumen_en;

        return $this;
    }

    public function getResumenFr(): ?string
    {
        return $this->resumen_fr;
    }

    public function setResumenFr(?string $resumen_fr): self
    {
        $this->resumen_fr = $resumen_fr;

        return $this;
    }

    public function getDescripcionEs(): ?string
    {
        return $this->descripcion_es;
    }

    public function setDescripcionEs(?string $descripcion_es): self
    {
        $this->descripcion_es = $descripcion_es;

        return $this;
    }

    public function getDescripcionEn(): ?string
    {
        return $this->descripcion_en;
    }

    public function setDescripcionEn(?string $descripcion_en): self
    {
        $this->descripcion_en = $descripcion_en;

        return $this;
    }

    public function getDescripcionFr(): ?string
    {
        return $this->descripcion_fr;
    }

    public function setDescripcionFr(?string $descripcion_fr): self
    {
        $this->descripcion_fr = $descripcion_fr;

        return $this;
    }

    public function getDetalleEs(): ?string
    {
        return $this->detalle_es;
    }

    public function setDetalleEs(?string $detalle_es): self
    {
        $this->detalle_es = $detalle_es;

        return $this;
    }

    public function getDetalleEn(): ?string
    {
        return $this->detalle_en;
    }

    public function setDetalleEn(?string $detalle_en): self
    {
        $this->detalle_en = $detalle_en;

        return $this;
    }

    public function getDetalleFr(): ?string
    {
        return $this->detalle_fr;
    }

    public function setDetalleFr(?string $detalle_fr): self
    {
        $this->detalle_fr = $detalle_fr;

        return $this;
    }

    public function getVariantesString(): ?string
    {
        $cad = '';
        foreach ($this->variantes as $variante){
            $cad .= " ".$variante->getTipo()." - ".$variante->getValor()." |";
        }
        $cad = rtrim($cad,"|");
        $cad = ltrim($cad," ");
        return $cad;
    }




}
