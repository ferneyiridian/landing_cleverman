<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompraRepository")
 */
class Compra
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", length=190)
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fecha_entrega;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $costo_domicilio;

    /**
     * @ORM\Column(type="float")
     */
    private $subtotal;

    /**
     * @ORM\Column(type="float")
     */
    private $total;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\EstadoPedido")
     * @ORM\JoinColumn(nullable=false)
     */
    private $estado;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MetodoPago")
     * @ORM\JoinColumn(nullable=true)
     */
    private $metodo;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $PagaCon;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Direccion")
     * @ORM\JoinColumn(nullable=false)
     */
    private $direccion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario")
     * @ORM\JoinColumn(nullable=false)
     */
    private $usuario;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fechaProcesamiento;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $estadoProcesamiento;

    /**
     * @ORM\Column(type="integer", nullable=true,options={"default":0})
     */
    private $intentos;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fechaRealProcesamiento;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $uuid_mu;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nombre_mensajero;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $status_mu;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $numero_mensajero;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $url_mapa;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="text")
     */
    private $comentarios;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Codigo", inversedBy="compras")
     */
    private $codigo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tipo_de_envio;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFechaEntrega(): ?\DateTimeInterface
    {
        return $this->fecha_entrega;
    }

    public function setFechaEntrega(?\DateTimeInterface $fecha_entrega): self
    {
        $this->fecha_entrega = $fecha_entrega;

        return $this;
    }

    public function getCostoDomicilio(): ?float
    {
        return $this->costo_domicilio;
    }

    public function setCostoDomicilio(float $costo_domicilio): self
    {
        $this->costo_domicilio = $costo_domicilio;

        return $this;
    }

    public function getSubtotal(): ?float
    {
        return $this->subtotal;
    }

    public function setSubtotal(float $subtotal): self
    {
        $this->subtotal = $subtotal;

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(float $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getEstado(): ?EstadoPedido
    {
        return $this->estado;
    }

    public function setEstado(?EstadoPedido $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getMetodo(): ?MetodoPago
    {
        return $this->metodo;
    }

    public function setMetodo(?MetodoPago $metodo): self
    {
        $this->metodo = $metodo;

        return $this;
    }

    public function getPagaCon(): ?float
    {
        return $this->PagaCon;
    }

    public function setPagaCon(?float $PagaCon): self
    {
        $this->PagaCon = $PagaCon;

        return $this;
    }

    public function getDireccion(): ?Direccion
    {
        return $this->direccion;
    }

    public function setDireccion(?Direccion $direccion): self
    {
        $this->direccion = $direccion;

        return $this;
    }

    public function getUsuario(): ?Usuario
    {
        return $this->usuario;
    }

    public function setUsuario(?Usuario $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getFechaProcesamiento(): ?\DateTimeInterface
    {
        return $this->fechaProcesamiento;
    }

    public function setFechaProcesamiento(?\DateTimeInterface $fechaProcesamiento): self
    {
        $this->fechaProcesamiento = $fechaProcesamiento;

        return $this;
    }

    public function getEstadoProcesamiento(): ?string
    {
        return $this->estadoProcesamiento;
    }

    public function setEstadoProcesamiento(?string $estadoProcesamiento): self
    {
        $this->estadoProcesamiento = $estadoProcesamiento;

        return $this;
    }

    public function getIntentos(): ?int
    {
        return $this->intentos;
    }

    public function setIntentos(?int $intentos): self
    {
        $this->intentos = $intentos;

        return $this;
    }

    public function getFechaRealProcesamiento(): ?\DateTimeInterface
    {
        return $this->fechaRealProcesamiento;
    }

    public function setFechaRealProcesamiento(?\DateTimeInterface $fechaRealProcesamiento): self
    {
        $this->fechaRealProcesamiento = $fechaRealProcesamiento;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUuidMu(): ?string
    {
        return $this->uuid_mu;
    }

    public function setUuidMu(?string $uuid_mu): self
    {
        $this->uuid_mu = $uuid_mu;

        return $this;
    }

    public function getNombreMensajero(): ?string
    {
        return $this->nombre_mensajero;
    }

    public function setNombreMensajero(?string $nombre_mensajero): self
    {
        $this->nombre_mensajero = $nombre_mensajero;

        return $this;
    }

    public function getStatusMu(): ?string
    {
        return $this->status_mu;
    }

    public function setStatusMu(?string $status_mu): self
    {
        $this->status_mu = $status_mu;

        return $this;
    }

    public function getNumeroMensajero(): ?string
    {
        return $this->numero_mensajero;
    }

    public function setNumeroMensajero(?string $numero_mensajero): self
    {
        $this->numero_mensajero = $numero_mensajero;

        return $this;
    }

    public function getUrlMapa(): ?string
    {
        return $this->url_mapa;
    }

    public function setUrlMapa(?string $url_mapa): self
    {
        $this->url_mapa = $url_mapa;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function __toString()
    {
        return "#" . $this->id;
    }

    public function getComentarios(): ?string
    {
        return $this->comentarios;
    }

    public function setComentarios(string $comentarios): self
    {
        $this->comentarios = $comentarios;

        return $this;
    }

    public function getCodigo(): ?Codigo
    {
        return $this->codigo;
    }

    public function setCodigo(?Codigo $codigo): self
    {
        $this->codigo = $codigo;

        return $this;
    }

    public function getTipoDeEnvio(): ?string
    {
        return $this->tipo_de_envio;
    }

    public function setTipoDeEnvio(?string $tipo_de_envio): self
    {
        $this->tipo_de_envio = $tipo_de_envio;

        return $this;
    }


}
