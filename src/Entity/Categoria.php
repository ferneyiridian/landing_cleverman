<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoriaRepository")
 */
class Categoria
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

     /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $resumen;

    /**
     * @ORM\Column(type="integer")
     */
    private $orden = 1;

    /**
     * @ORM\Column(type="boolean")
     */
    private $visible = true;

     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Image", inversedBy="categorias", cascade={"persist"})
     * @ORM\JoinColumn(referencedColumnName="id",nullable=false)
     */
    private $imagen;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Segmento", inversedBy="categorias")
     */
    private $segmento;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SubCategoriaBlog", mappedBy="categoria")
     */
    private $subCategoriaBlogs;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Journey", mappedBy="categoria")
     */
    private $journeys;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nombre_es;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nombre_en;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nombre_fr;



    public function __construct()
    {
       
        $this->journeys = new ArrayCollection();
        $this->subCategoriaBlogs = new ArrayCollection();

    }

    public function __toString()
    {
        return $this->getNombre().' ';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        $locale = $GLOBALS['request']->getLocale();
        $field = 'nombre_'.$locale;
        return $this->{$field};
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getResumen(): ?string
    {
        return $this->resumen;
    }

    public function setResumen(?string $resumen): self
    {
        $this->resumen = $resumen;

        return $this;
    }

    public function getImagen(): ?Image
    {
        return $this->imagen;
    }

    public function setImagen(?Image $imagen): self
    {
        $this->imagen = $imagen;
        $this->imagen->addCategoria($this);

        return $this;
    }

    public function getOrden(): ?int
    {
        return $this->orden;
    }

    public function setOrden(int $orden): self
    {
        $this->orden = $orden;

        return $this;
    }

    public function getVisible(): ?bool
    {
        return $this->visible;
    }

    public function setVisible(bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }



    /**
     * @return Collection|Producto[]
     */
    public function getProductos(): Collection
    {
        return $this->productos;
    }

    public function addProducto(Producto $producto): self
    {
        if (!$this->productos->contains($producto)) {
            $this->productos[] = $producto;
            $producto->addCategoria($this);
        }

        return $this;
    }

    public function removeProducto(Producto $producto): self
    {
        if ($this->productos->contains($producto)) {
            $this->productos->removeElement($producto);
            $producto->removeCategoria($this);
        }

        return $this;
    }


    public function getSegmento(): ?Segmento
    {
        return $this->segmento;
    }

    public function setSegmento(?Segmento $segmento): self
    {
        $this->segmento = $segmento;

        return $this;
    }

    /**
     * @return Collection|SubCategoriaBlog[]
     */
    public function getSubCategoriaBlogs(): Collection
    {
        return $this->subCategoriaBlogs;
    }

    public function addSubCategoriaBlog(SubCategoriaBlog $subCategoriaBlog): self
    {
        if (!$this->subCategoriaBlogs->contains($subCategoriaBlog)) {
            $this->subCategoriaBlogs[] = $subCategoriaBlog;
            $subCategoriaBlog->setCategoria($this);
        }

        return $this;
    }

    public function removeSubCategoriaBlog(SubCategoriaBlog $subCategoriaBlog): self
    {
        if ($this->subCategoriaBlogs->contains($subCategoriaBlog)) {
            $this->subCategoriaBlogs->removeElement($subCategoriaBlog);
            // set the owning side to null (unless already changed)
            if ($subCategoriaBlog->getCategoria() === $this) {
                $subCategoriaBlog->setCategoria(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Journey[]
     */
    public function getJourneys(): Collection
    {
        return $this->journeys;
    }

    public function addJourney(Journey $journey): self
    {
        if (!$this->journeys->contains($journey)) {
            $this->journeys[] = $journey;
            $journey->setCategoria($this);
        }

        return $this;
    }

    public function removeJourney(Journey $journey): self
    {
        if ($this->journeys->contains($journey)) {
            $this->journeys->removeElement($journey);
            // set the owning side to null (unless already changed)
            if ($journey->getCategoria() === $this) {
                $journey->setCategoria(null);
            }
        }

        return $this;
    }

    public function getNombreEs(): ?string
    {
        return $this->nombre_es;
    }

    public function setNombreEs(?string $nombre_es): self
    {
        $this->nombre_es = $nombre_es;

        return $this;
    }

    public function getNombreEn(): ?string
    {
        return $this->nombre_en;
    }

    public function setNombreEn(?string $nombre_en): self
    {
        $this->nombre_en = $nombre_en;

        return $this;
    }

    public function getNombreFr(): ?string
    {
        return $this->nombre_fr;
    }

    public function setNombreFr(?string $nombre_fr): self
    {
        $this->nombre_fr = $nombre_fr;

        return $this;
    }


}
