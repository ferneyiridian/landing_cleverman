<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TextoBigRepository")
 */
class TextoBig
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $llave;

    /**
     * @ORM\Column(type="text")
     */
    private $valor_es;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $valor_en;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $valor_fr;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLlave(): ?string
    {
        return $this->llave;
    }

    public function setLlave(string $llave): self
    {
        $this->llave = $llave;

        return $this;
    }

    public function getValor(): ?string
    {
        $locale = $GLOBALS['request']->getLocale();
        $field = 'valor_'.$locale;
        return $this->{$field};
    }

    public function setValor(string $valor): self
    {
        $this->valor = $valor;

        return $this;
    }

    public function getValorEs(): ?string
    {
        return $this->valor_es;
    }

    public function setValorEs(?string $valor_es): self
    {
        $this->valor_es = $valor_es;

        return $this;
    }

    public function getValorEn(): ?string
    {
        return $this->valor_en;
    }

    public function setValorEn(string $valor_en): self
    {
        $this->valor_en = $valor_en;

        return $this;
    }

    public function getValorFr(): ?string
    {
        return $this->valor_fr;
    }

    public function setValorFr(?string $valor_fr): self
    {
        $this->valor_fr = $valor_fr;

        return $this;
    }
}
