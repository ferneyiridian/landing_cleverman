<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ServicioRepository")
 */
class Servicio
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="text")
     */
    private $resumen;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Image", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $imagen;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Image", mappedBy="servicio", cascade={"persist"})
     */
    private $galeria;

    public function __construct()
    {
        $this->galeria = new ArrayCollection();
        $this->imagen = new Image();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getResumen(): ?string
    {
        return $this->resumen;
    }

    public function setResumen(string $resumen): self
    {
        $this->resumen = $resumen;

        return $this;
    }

    public function getImagen(): ?Image
    {
        return $this->imagen;
    }

    public function setImagen(?Image $imagen): self
    {
        $this->imagen = $imagen;
        $this->imagen->setServicio($this);

        return $this;
    }

    /**
     * @return Collection|Image[]
     */
    public function getGaleria(): Collection
    {
        return $this->galeria;
    }

    public function addGalerium(Image $galerium): self
    {
        if (!$this->galeria->contains($galerium)) {
            $this->galeria[] = $galerium;
            $galerium->setServicio($this);
        }

        return $this;
    }

    public function removeGalerium(Image $galerium): self
    {
        if ($this->galeria->contains($galerium)) {
            $this->galeria->removeElement($galerium);
            // set the owning side to null (unless already changed)
            if ($galerium->getServicio() === $this) {
                $galerium->setServicio(null);
            }
        }

        return $this;
    }
}
