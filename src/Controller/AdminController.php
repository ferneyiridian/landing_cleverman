<?php
/**
 * Created by PhpStorm.
 * User: Mauricio
 * Date: 25/04/2019
 * Time: 7:25 PM
 */

namespace App\Controller;

use AlterPHP\EasyAdminExtensionBundle\Controller\EasyAdminController;
use AlterPHP\EasyAdminExtensionBundle\Security\AdminAuthorizationChecker;
use App\Entity\Ciudad;
use App\Entity\Color;
use App\Entity\Especialidad;
use App\Entity\Masivo;
use App\Entity\Medico;
use App\Entity\PagoSuscripcion;
use App\Entity\Producto;
use App\Entity\Suscripcion;
use App\Entity\Usuario;
use App\Service\QI;
use Doctrine\ORM\Query\ResultSetMapping;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Exception;
use FOS\UserBundle\Model\UserManagerInterface;
#use function Google\Cloud\Samples\Datastore\properties;
#use Kreait\Firebase;
use mysqli;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AdminController extends EasyAdminController
{
    private $validator;
    private $firebase;
    private $userManager;
    private $qi;


    /**
     * AdminController constructor.
     */
    public function __construct(QI $qi, UserManagerInterface $userManager)
    {
        $this->qi = $qi;
        $this->userManager = $userManager;

        //$this->firebase = $firebase;

    }

    /**
     * @Route("/sincronizar_suscripciones_pagas", name="sincronizar_suscripciones_pagas")
     */
    public function sincronizarSuscripcionesPagas(Request $request)
    {

        $em= $this->getDoctrine()->getManager();
        $limbo=[];
        $suscripcion = $this->getDoctrine()->getManager()->createQueryBuilder()
            ->from('App:Suscripcion','s')
            ->select('s')
            ->leftJoin('s.pagos','p')
            ->addSelect('p')
            ->where('s.activa = 0 ')
            //->setParameter('padre',null)
            ->getQuery()
            ->getArrayResult();
        foreach ($suscripcion as $s ){
            $pagos = $s['pagos'];
            if( count($pagos)== 0  && $s['id_payu']){
                array_push($limbo,$s);
            }
        }
        $activas = [];
        $rechazados = [];
        foreach ($limbo as $s){
            $activa = false;
            $resp = $this->qi->getPagoSuscripcionPayu($s['id_payu']);
            $sus = $this->getDoctrine()->getRepository(Suscripcion::class)->find($s['id']);
            if(isset($resp->recurringBillList)) {
                foreach ($resp->recurringBillList as $p) {
                    $pago = new PagoSuscripcion();
                    $pago->setIdPayu($p->id);
                    $pago->setOrderId($p->subscriptionId);
                    $pago->setMonto($p->amount);
                    $pago->setMoneda($p->currency);
                    $fecha = new \DateTime();
                    $fecha->setTimestamp($p->dateCharge);
                    $pago->setFecha($fecha);
                    $pago->setState($p->state);
                    $pago->setSuscripcion($sus );
                    $em->persist($pago);
                    $em->flush();
                    $this->qi->saveFire($pago);
                    if ($p->state == 'completed' || $p->state == 'PAID') {
                        $activa = $activa || true;
                    }else{
                        $activa = $activa || false;
                    }
                }
            }else{
                $pago = new PagoSuscripcion();
                $pago->setIdPayu("");
                $pago->setOrderId("");
                $pago->setMonto(0);
                $pago->setMoneda("COP");
                $fecha = new \DateTime();
                $pago->setFecha($fecha);
                $pago->setState("EXPIRED");
                $pago->setSuscripcion($sus );
                $em->persist($pago);
                $em->flush();
            }
            if($activa){
                $sus->setActiva(1);
                $em->persist($sus);
                $em->flush();
                $sus->setEmail("");
                $sus->setDuracion(0);
                $sus->setIdPayu("");
                $sus->setPayerId("");
                $sus->setIdPaypal("");
                $sus->setPlanId("");
                $sus->setUsuario(null);
                $sus->setValidaHasta(null);
                $this->qi->saveFire($sus);
                array_push($activas, $s['id']);
            }else{
                array_push($rechazados, $s['id']);
            }

        }


        return $this->render('basic/sincronizarPayu.html.twig', [
            'activas'=>$activas,
            'rechazadas'=>$rechazados
        ]);

    }

    public function reembolsarAction()
    {
        // controllers extending the base AdminController get access to the
        // following variables:
        //   $this->request, stores the current request
        //   $this->em, stores the Entity Manager for this Doctrine entity

        // change the properties of the given entity and save the changes
        $id = $this->request->query->get('id');
        $entity = $this->em->getRepository(Suscripcion::class)->find($id);
        $pago = $this->em->getRepository(PagoSuscripcion::class)->findOneBy(array('suscripcion'=>$entity,'state'=>'APPROVED'),array('id'=>'desc'));
        $refund = $this->qi->makeRefundPayu($pago,$entity);
        if($refund ) {
            if($refund->code == 'SUCCESS') {

                $entity->setActiva(0);
                $entity->setReembolsada(true);
                $entity->setFechaReembolso(new \DateTime());
                $this->em->persist($entity);

                $pago->setState('REFUND_REQUESTED');
                $this->em->persist($entity);

                $this->em->flush();
                $this->addFlash('success', 'Solicitud de reembolso enviada');
            }else{
                $this->addFlash('error', $refund->error);
            }


        }else{
            $this->addFlash('error', 'Ha ocurrido un error, revisa que sea una transacción de PAYU e intenta de nuevo');
            //dd('no pago');
        }
        // redirect to the 'list' view of the given entity ...
        return $this->redirectToRoute('easyadmin', array(
            'action' => 'list',
            'usuario' => $entity->getUsuario()->getId(),
            'entity' => $this->request->query->get('entity'),
        ));

    }


    public function listSuscripcionDobleAction()
    {

        $em= $this->getDoctrine()->getManager();
        $suscripciones = $this->getDoctrine()->getManager()->createQueryBuilder()
            ->from('App:Suscripcion','s')
            ->select('s')
            ->leftJoin('s.pagos','p')
            ->addSelect('p')
            ->leftJoin('s.usuario','u')
            ->addSelect('u')
            ->where('s.activa = 1 and s.id_payu is not null')
            //->setParameter('padre',null)
            ->getQuery()
            ->getArrayResult();

        $users =[];
        $duplicados=[];
        foreach ($suscripciones as $s){

            if(isset($users[$s['usuario']['id']]) ){
                $users[$s['usuario']['id']] =array('usuario'=>$s['usuario'],'cant'=>$users[$s['usuario']['id']]['cant']+1 );
            }else{
                $users[$s['usuario']['id']]=array('usuario'=>$s['usuario'],'cant'=>1 );
            }

        }
        foreach ($users as $k=>$u){
            if($u['cant']>1){
                $duplicados[$k]=$u;
            }

        }
        //dump($duplicados);



        $this->dispatch(EasyAdminEvents::PRE_LIST);

        $fields = $this->entity['list']['fields'];
        $this->entity['list']['dql_filter'] = "entity.id in (". join(',',array_keys($duplicados)).')';
        $paginator = $this->findAll($this->entity['class'], $this->request->query->get('page', 1), $this->entity['list']['max_results'], $this->request->query->get('sortField'), $this->request->query->get('sortDirection'), $this->entity['list']['dql_filter']);

        $this->dispatch(EasyAdminEvents::POST_LIST, ['paginator' => $paginator]);

        //dump($this->entity['list']['dql_filter']);
        //die();

        $parameters = [
            'paginator' => $paginator,
            'fields' => $fields,
            'batch_form' => $this->createBatchForm($this->entity['name'])->createView(),
            'delete_form_template' => $this->createDeleteForm($this->entity['name'], '__id__')->createView(),
        ];

        return $this->executeDynamicMethod('render<EntityName>Template', ['list', $this->entity['templates']['list'], $parameters]);



    }


    protected function initialize(Request $request)
    {
        parent::initialize($request);
    }

    public function createNewUsuarioEntity()
    {
        return $this->userManager->createUser();
    }

    public function persistUsuarioEntity($user)
    {
        $this->userManager->updateUser($user, false);
        parent::persistEntity($user);
    }

    public function updateUsuarioEntity($user)
    {
        $this->userManager->updateUser($user, false);
        parent::updateEntity($user);
    }



    protected function listSuscripcionAction()
    {
        $this->dispatch(EasyAdminEvents::PRE_LIST);

        $fields = $this->entity['list']['fields'];
        $usuario = $this->request->query->get('usuario', null);
        if($usuario){
            $this->entity['list']['dql_filter'] = "entity.usuario = ".$usuario;
        }else{
            $this->entity['list']['dql_filter'] = "";
        }
        $paginator = $this->findAll($this->entity['class'], $this->request->query->get('page', 1), $this->entity['list']['max_results'], $this->request->query->get('sortField'), $this->request->query->get('sortDirection'), $this->entity['list']['dql_filter']);

        $this->dispatch(EasyAdminEvents::POST_LIST, ['paginator' => $paginator]);

        //dump($this->entity['list']['dql_filter']);
        //die();

        $parameters = [
            'paginator' => $paginator,
            'fields' => $fields,
            'batch_form' => $this->createBatchForm($this->entity['name'])->createView(),
            'delete_form_template' => $this->createDeleteForm($this->entity['name'], '__id__')->createView(),
        ];

        return $this->executeDynamicMethod('render<EntityName>Template', ['list', $this->entity['templates']['list'], $parameters]);
    }

    protected function listPagoSuscripcionAction()
    {
        $this->dispatch(EasyAdminEvents::PRE_LIST);

        $fields = $this->entity['list']['fields'];
        $suscripcion = $this->request->query->get('suscripcion', null);
        if($suscripcion){
            $this->entity['list']['dql_filter'] = "entity.suscripcion = ".$suscripcion;
        }else{
            $this->entity['list']['dql_filter'] = "";
        }
        $paginator = $this->findAll($this->entity['class'], $this->request->query->get('page', 1), $this->entity['list']['max_results'], $this->request->query->get('sortField'), $this->request->query->get('sortDirection'), $this->entity['list']['dql_filter']);

        $this->dispatch(EasyAdminEvents::POST_LIST, ['paginator' => $paginator]);

        //dump($this->entity['list']['dql_filter']);
        //die();

        $parameters = [
            'paginator' => $paginator,
            'fields' => $fields,
            'batch_form' => $this->createBatchForm($this->entity['name'])->createView(),
            'delete_form_template' => $this->createDeleteForm($this->entity['name'], '__id__')->createView(),
        ];

        return $this->executeDynamicMethod('render<EntityName>Template', ['list', $this->entity['templates']['list'], $parameters]);
    }






    public function mailAprobacion($asunto, $llave,$contenido_mail){
        $qi = $this->qi;
        $html = $qi->getTextoBig('mail_base');
        $html = str_replace(['(contenido)']
            ,[
                $contenido_mail
            ]
            ,$html);
        $mails = $qi->getSetting($llave);
        foreach (explode(',',$mails) as $mail){
            $qi->sendMailIB('RECORDATI | '.$asunto,$mail,$html);
        }
    }




    private function reporteNativoGenORM($cols,$cadena){
        $rsm = new ResultSetMapping();
        foreach ($cols as $col){
            $rsm->addScalarResult($col, $col, 'string');
        }
        $reporte = $this->getDoctrine()->getManager()->createNativeQuery($cadena, $rsm)->getArrayResult();
        return $reporte;
    }

}
