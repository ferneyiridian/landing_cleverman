git pull
docker stop sf4_apache sf4_phpmyadmin sf4_php sf4_mysql sf4_maildev
npm i
yarn encore dev
cd public && npm i
cd ..
docker-compose up -d --build
sleep 5
docker exec -it -w /home/wwwroot/sf4 sf4_php composer install
docker exec -it -w /home/wwwroot/sf4 sf4_php php bin/console assets:install
docker exec -it -w /home/wwwroot/sf4 sf4_php chmod -R 777 var
docker exec -it -w /home/wwwroot/sf4 sf4_php php bin/console cache:clear
docker exec -it -w /home/wwwroot/sf4 sf4_php php bin/console cache:clear --env prod
docker exec -it -w /home/wwwroot/sf4 sf4_php php bin/console cache:warmup
docker exec -it -w /home/wwwroot/sf4 sf4_php php bin/console cache:warmup --env prod
#php bin/console cache:clear && php bin/console cache:clear --env prod && chmod -R 777 var/
